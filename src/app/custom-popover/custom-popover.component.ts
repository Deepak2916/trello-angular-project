import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-custom-popover',
  templateUrl: './custom-popover.component.html',
  styleUrls: ['./custom-popover.component.css']
})
export class CustomPopoverComponent {
  @Input() title: string = 'Popover Title';
  @Output() submitClick: EventEmitter<string> = new EventEmitter<string>();
  isPopoverOpen:boolean = false

  textFieldValue: string = '';

  onButtonClick() {
    if(this.textFieldValue!==''){

      this.submitClick.emit(this.textFieldValue);
      this.isPopoverOpen = false
    }
    this.textFieldValue = ''; 
  }
  togglePopover() {
    this.isPopoverOpen = !this.isPopoverOpen;
  }
}
