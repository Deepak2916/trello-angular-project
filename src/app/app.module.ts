import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BoardsComponent } from './boards/boards.component';
import { HttpClientModule } from '@angular/common/http';
import { CustomPopoverComponent } from './custom-popover/custom-popover.component';
import { FormsModule } from '@angular/forms';
import { ListsComponent } from './lists/lists.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent, BoardsComponent, CustomPopoverComponent, ListsComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule,FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
