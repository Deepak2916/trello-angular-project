import { Component, Input, OnInit } from '@angular/core';
import { TrelloApiService } from '../trello-api.service';
@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css'],
})
export class BoardsComponent implements OnInit {
  boards: any;
  areBoardsLeft: boolean = true;

  constructor(private TrelloApiService: TrelloApiService) {}

  ngOnInit(): void {
    this.TrelloApiService.getRequest('members/me/boards?').subscribe(
      (response) => {
        this.boards = response;
      }
    );
  }

  createNewBoard(newBoardName: string) {
    this.TrelloApiService.postRequest(
      `boards/?name=${newBoardName}&`
    ).subscribe((response) => {
      this.boards.push(response);
      if(this.boards.length===10){
        this.areBoardsLeft = false
      }
    });
  }
}
