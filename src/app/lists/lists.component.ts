import { Component, OnInit } from '@angular/core';
import { TrelloApiService } from '../trello-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css'],
})
export class ListsComponent implements OnInit {
  lists: any;
  cards: any;

  constructor(
    private TrelloApiService: TrelloApiService,
    private route: ActivatedRoute
  ) {}
  boardId: string | null = '';
  boardName: string | null = '';

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.boardId = params.get('id');
      this.boardName = params.get('boardName');
    });

    this.TrelloApiService.getRequest(`boards/${this.boardId}/lists?`).subscribe(
      (response) => {
        this.lists = response;
   
      }
    );
    this.TrelloApiService.getRequest(`boards/${this.boardId}/cards?`).subscribe(
      (response) => {
        console.log(response);
        
        this.cards = response;
      }
    );

   
  }
  createNewList(title: string) {
    this.TrelloApiService.postRequest(
      `boards/${this.boardId}/lists?name=${title}&`
    ).subscribe((response) => {
      this.lists.push(response);
    });
  }

  createNewCard(title:string,listId:string){
    this.TrelloApiService.postRequest(
      `cards?idList=${listId}&name=${title}&`
    ).subscribe((response) => {
      this.cards.push(response);
    });
    
  }

  deleteList(id:string){
    this.TrelloApiService.updateResquest(`lists/${id}?closed=true&`).subscribe(()=>{
      this.lists = this.lists.filter((list:any)=>{
        return list.id !== id
      })
    })
  }
  deleteCard(id:string){
    // cards/${id}?
    this.TrelloApiService.deleteRequest(`cards/${id}?`).subscribe(()=>{
      this.cards = this.cards.filter((card:any)=>{
        return card.id !== id
      })
    })
  }
}
