import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TrelloApiService {
  constructor(private http: HttpClient) {}

  getRequest(mainPath:string): Observable<any> {
    return this.http.get(environment.apiUrl(mainPath));
  }
  postRequest(mainPath:string): Observable<any> {
    return this.http.post(environment.apiUrl(mainPath),'');
  }
  deleteRequest(mainPath:string): Observable<any> {
    return this.http.delete(environment.apiUrl(mainPath));
  }
  updateResquest(mainPath:string): Observable<any> {
    return this.http.put(environment.apiUrl(mainPath),'');
  }
}
